const Uploader = require('../')
const path = require('path')

setImmediate(async () => {
    const uploader = new Uploader({
        credentials: path.join(__dirname, '../../codeatest-api/src/services/code-and-test-1a7e7a4f4c05.json'), projectId: 'blissful-star-290200', bucketName: 'codeatest-2'
    })

    try {

        // upload
        const result = await uploader.uploadFile(path.join(__dirname, '../index.js'), {clean: false, destination: 'code/js/test.js'})

        // console.log(result)

        const signedUrl = await uploader.generateSignedUrl('code/js/test.js')

        console.log(signedUrl)

        process.exit(0)
    } catch (e) {

        console.log(e)

        process.exit(1)
    }
})
