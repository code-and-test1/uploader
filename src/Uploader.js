const GoogleStorage = require('./GoogleStorage')
const fs = require('fs')

const rm = (filePath) => {
    return new Promise((rs, rj) => {
        fs.unlink(filePath, (error) => {
            if (error) rj(error)

            rs(filePath)
        })
    })
}

class Uploader {
    constructor(options = {}) {
        this.bucket = GoogleStorage.getBucket(options)
    }

    getBucket() {
        return this.bucket
    }

    async uploadFile(filePath, options = {}) {
        const {clean, destination} = options

        delete options.clean

        try {

            if (!filePath) throw new Error(`Missing filePath to upload.`)

            await this.bucket.upload(filePath, options)

            if (clean) {
                await rm(filePath)
            }

            return destination || filePath
        } catch (error) {
            if (clean && filePath) {
                await rm(filePath)
            }

            throw error
        }

    }

    async generateSignedUrl(file, options = {}) {

        if (!file) throw new Error(`Missing file to read.`)

        const dOptions = {
            version: 'v2', // defaults to 'v2' if missing.
            action: 'read',
            expires: Date.now() + 1000 * 60 * 60 * 24, // one day
        }

        // Get a v2 signed URL for the file
        const [url] = await this.bucket
            .file(file)
            .getSignedUrl(Object.assign(dOptions, options))

        return url
    }

}

module.exports = Uploader
