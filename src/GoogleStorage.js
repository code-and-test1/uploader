// Imports the Google Cloud client library
const {Storage} = require('@google-cloud/storage');
const path = require('path')

const DEFAULT_CREDENTIALS = path.join(__dirname, './code-and-test-1a7e7a4f4c05.json')
const DEFAULT_PROJECT_ID = 'code-and-t'
const DEFAULT_BUCKET_NAME = 'codeatest'

// Creates a client from a Google service account key.

exports.getBucket = ({credentials, projectId, bucketName}) => {

    const storage = new Storage({keyFilename: credentials || DEFAULT_CREDENTIALS, projectId: projectId || DEFAULT_PROJECT_ID});

    return storage.bucket(bucketName || DEFAULT_BUCKET_NAME)
}

